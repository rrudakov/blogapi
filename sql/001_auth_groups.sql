CREATE TABLE auth_groups (
        id SERIAL NOT NULL UNIQUE,
        group_name VARCHAR(30) NOT NULL UNIQUE
        );

INSERT INTO auth_groups (id, group_name) VALUES
       (1, 'admin'),
       (2, 'moderator'),
       (3, 'user'),
       (4, 'guest');
