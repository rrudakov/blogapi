CREATE TABLE profiles (
        user_id INTEGER REFERENCES users (id) ON DELETE RESTRICT,
        first_name VARCHAR(50),
        last_name VARCHAR(50),
        birth_date DATE,
        avatar VARCHAR(500),
        PRIMARY KEY (user_id)
        );
