-- | Some utilities for application

module Utils.Helpers
  (
    -- * options for deriving JSON
    jsonOptions
  ) where

import           Data.Aeson (Options, camelTo2, defaultOptions,
                             fieldLabelModifier)

-- | JSON deriving options
jsonOptions :: Options
jsonOptions = defaultOptions {fieldLabelModifier = camelTo2 '_'}
