{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
-- | Main application run module
module App
  ( runApp
  ) where

import           App.Config
import           App.Logging
import           App.Route                                 (api)
import           App.Server                                (server)
import           Control.Monad.Reader
import           Data.Pool
import           Data.Time                                 (getCurrentTime)
import           Data.Yaml.Config
import           Database.PostgreSQL.Simple                (ConnectInfo,
                                                            Connection, close,
                                                            connect,
                                                            withTransaction)
import           Database.PostgreSQL.Simple.Migration      (MigrationCommand (..),
                                                            MigrationContext (..),
                                                            runMigration)
import           Debug.Trace
import           Network.Wai                               (Middleware)
import           Network.Wai.Handler.Warp                  as Warp
import           Network.Wai.Middleware.RequestLogger
import           Network.Wai.Middleware.RequestLogger.JSON
import           Servant
import           Servant.Auth.Server
import           System.Log.FastLogger

port :: Int
port = 8080

app :: Context '[ CookieSettings, JWTSettings]
  -> CookieSettings
  -> JWTSettings
  -> AppCtx
  -> Application
app cfg cs jwts ctx =
  serveWithContext api cfg $
  hoistServerWithContext
    api
    (Proxy :: Proxy '[ CookieSettings, JWTSettings])
    (`runReaderT` ctx)
    (server cs jwts)

initConnectionPool :: ConnectInfo -> IO (Pool Connection)
initConnectionPool connect_info =
  createPool
    (connect connect_info)
    close
    20 -- stripes
    60 -- unused connections are kept open for a minute
    10 -- max. 10 connections open per stripe

jsonRequestLogger :: IO Middleware
jsonRequestLogger =
  mkRequestLogger $
  def {outputFormat = CustomOutputFormatWithDetails dontLogHealthEndpoint}

dontLogHealthEndpoint :: OutputFormatterWithDetails
dontLogHealthEndpoint date req status responseSize duration reqBody response =
  traceShow reqBody $
  traceShow req $
  formatAsJSON date req status responseSize duration reqBody response

runApp :: IO ()
runApp = do
  config <- loadYamlSettings ["config.yml"] [] ignoreEnv

  -- Init database connection
  let dbConfig = databaseConfig config
  pool <- initConnectionPool $ appConnectionInfo dbConfig

  -- Init migrations
  _ <-
    withResource pool $ \conn ->
      withTransaction conn $
      runMigration $ MigrationContext MigrationInitialization True conn

  -- Run database migrations
  _ <-
    withResource pool $ \conn ->
      withTransaction conn $
      runMigration $ MigrationContext (MigrationDirectory "sql") True conn

  -- Initialize loggers
  warpLogger <- jsonRequestLogger
  appLogger <- newStdoutLoggerSet defaultBufSize

  -- Log application start
  tstamp <- getCurrentTime
  appKey <- generateKey
  let lgmsg =
        LogMessage
          { message = "Blog app starting up!"
          , timestamp = tstamp
          , level = "info"
          , lversion = appVersion config
          , lenvironment = appEnvironment config
          }
  pushLogStrLn appLogger (toLogStr lgmsg) >> flushLogStr appLogger

  -- Init application context and run server
  let ctx = AppCtx config appLogger pool
      warpSettings = Warp.defaultSettings
      portSettings = Warp.setPort port warpSettings
      settings = Warp.setTimeout 55 portSettings
      jwtCfg = defaultJWTSettings appKey
      cookieCfg = if appEnvironment config == "dev"
                  then defaultCookieSettings{cookieIsSecure=NotSecure}
                  else defaultCookieSettings
      cfg = cookieCfg :. jwtCfg :. EmptyContext

  Warp.runSettings settings $
    warpLogger $ app cfg cookieCfg jwtCfg ctx
