{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE InstanceSigs      #-}
{-# LANGUAGE OverloadedStrings #-}
-- | All application errors

module App.Error where

import           App.Config           (AppM)
import           Control.Monad.Except (MonadError)
import           Data.Aeson
import           Network.HTTP.Types   (hContentType)
import           Servant

throwJSONError ::
     (MonadError ServerError m, ToJSON a) => ServerError -> a -> m b
throwJSONError err j =
  throwError $ err {errBody = encode j, errHeaders = [jsonHeader]}
  where
    jsonHeader = (hContentType, "application/json;charset=utf-8")

-- | Define all possible errors from our application
data AppError
  = InternalServerError
  | DataBaseError
  | WrongPasswordError
  | BadRequestError
  | UserNotFountError
  | IncorrectLoginError
  | LoginUsedError
  | ProfileExistError
  | GroupNotFoundError
  | PasswordTooShortError
  | AccessDeniedError
  | UnauthorizedError

instance Show AppError where
  show InternalServerError   = "Something went wrong"
  show DataBaseError         = "Unable to execute query from database"
  show BadRequestError       = "Check request body and API endpoint"
  show WrongPasswordError    = "Incorrect password"
  show UserNotFountError     = "User not found"
  show IncorrectLoginError   = "Login incorrect"
  show LoginUsedError        = "This login already used, try another"
  show ProfileExistError     = "Unable to create user profile"
  show PasswordTooShortError = "Password should be as least 6 symbols"
  show AccessDeniedError     = "Access denied"
  show UnauthorizedError     = "Authorization required"
  show GroupNotFoundError    = "Required authorization group not found"

instance ToJSON AppError where
  toJSON e = object ["error" .= show e]
  toEncoding e = pairs ("error" .= show e)

-- |Throw JSON error with right error code
throwErr :: AppError -> AppM a
throwErr InternalServerError   = throwJSONError err500 InternalServerError
throwErr DataBaseError         = throwJSONError err500 DataBaseError
throwErr BadRequestError       = throwJSONError err400 BadRequestError
throwErr WrongPasswordError    = throwJSONError err400 WrongPasswordError
throwErr UserNotFountError     = throwJSONError err404 UserNotFountError
throwErr IncorrectLoginError   = throwJSONError err400 IncorrectLoginError
throwErr LoginUsedError        = throwJSONError err409 LoginUsedError
throwErr ProfileExistError     = throwJSONError err500 ProfileExistError
throwErr GroupNotFoundError    = throwJSONError err404 GroupNotFoundError
throwErr PasswordTooShortError = throwJSONError err400 PasswordTooShortError
throwErr AccessDeniedError     = throwJSONError err403 AccessDeniedError
throwErr UnauthorizedError     = throwJSONError err401 UnauthorizedError

-- |Handle result of model functions
handleIOResult :: Either AppError a -> AppM a
handleIOResult (Right a) = return a
handleIOResult (Left e)  = throwErr e
