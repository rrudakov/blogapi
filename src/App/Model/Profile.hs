{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
-- | Work with user profile in database

module App.Model.Profile
  (
    -- * The @UserProfile@ type
    UserProfile
    -- * profile manage functions
  , createEmptyProfile
  , getProfileById
  , updateProfile
  ) where

import           App.Error
import           Control.Exception.Base            (handleJust)
import           Data.Aeson.TH
import           Data.Pool
import           Data.Swagger
import qualified Data.Text                         as T
import           Data.Time.Calendar                (Day)
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.Errors (ConstraintViolation (..),
                                                    constraintViolation)
import           GHC.Generics
import           Servant                           (NoContent (..))
import           Utils.Helpers                     (jsonOptions)

-- | Profile data type
data UserProfile = UProfile
  { userId    :: Int
  , firstName :: Maybe T.Text
  , lastName  :: Maybe T.Text
  , birthDate :: Maybe Day
  , avatar    :: Maybe T.Text
  } deriving (Show, Generic)

$(deriveJSON jsonOptions ''UserProfile)

instance FromRow UserProfile
instance ToSchema UserProfile

-- | Create user profile
createEmptyProfile :: Pool Connection
                   -> Int -- ^ user id
                   -> IO (Either AppError NoContent)
createEmptyProfile conns uid =
  handleJust constraintViolation handler $ do
    let q = "INSERT INTO profiles (user_id) VALUES (?) RETURNING user_id"
    _ <- withResource conns $ \conn -> query conn q [uid] :: IO [Only Int]
    return $ Right NoContent
  where
    handler (ForeignKeyViolation "profiles" "profiles_user_id_fkey") =
      return $ Left UserNotFountError
    handler (UniqueViolation "profiles_pkey") = return $ Left ProfileExistError
    handler _ = return $ Left DataBaseError

-- | Get profile by user id
getProfileById :: Pool Connection
               -> Int -- ^ user id
               -> IO (Either AppError UserProfile)
getProfileById conns uid = do
  let q = "SELECT * FROM profiles WHERE user_id = ?"
  res <- withResource conns $ \conn -> query conn q [uid]
  case res of
    [profile] -> return $ Right profile
    []        -> return $ Left UserNotFountError
    _         -> return $ Left DataBaseError

-- | Update profile
updateProfile :: Pool Connection
              -> Int -- ^ user id
              -> UserProfile -- ^ request body
              -> IO (Either AppError NoContent)
updateProfile conns uid UProfile {..}
  | uid /= userId = return $ Left BadRequestError
  | otherwise = do
    let q =
          "UPDATE profiles SET first_name = ?, last_name = ?,\
              \ birth_date = ?, avatar = ? WHERE user_id = ?"
    res <-
      withResource conns $ \conn ->
        execute conn q (firstName, lastName, birthDate, avatar, userId)
    case res of
      1 -> return $ Right NoContent
      0 -> return $ Left UserNotFountError
      _ -> return $ Left DataBaseError
