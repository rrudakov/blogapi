{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
-- | All database staff related to authentication and registration

module App.Model.Auth
  (
    -- * The @AuthData@ type
    AuthData (..)
    -- * The @TokenResponse@ type
  , TokenResponse (..)
    -- * The @Credentials@ type
  , Credentials
    -- * user manage functions
  , authCheck
  , createUser
    -- * auth helpers
  , withGroup
  ) where

import           App.Error
import           App.Model.Profile                 (createEmptyProfile)
import           Control.Exception.Base            (handleJust)
import           Crypto.BCrypt
import           Data.Aeson                        (FromJSON)
import           Data.Aeson.TH
import qualified Data.ByteString.Char8             as C
import           Data.Pool
import           Data.Swagger
import qualified Data.Text                         as T
import           Data.Time                         (UTCTime)
import           Database.PostgreSQL.Simple
import           Database.PostgreSQL.Simple.Errors (ConstraintViolation (..),
                                                    constraintViolation)
import           GHC.Generics
import           Servant.Auth.Server               (AuthResult (..), FromJWT,
                                                    ToJWT)
import           Utils.Helpers                     (jsonOptions)

-- | This data will be encrypted via JWT.
-- All data will be available from outside
data AuthData = AData
  { authId    :: Int
  , authGroup :: Int
  } deriving (Show, Generic)

$(deriveJSON jsonOptions ''AuthData)

instance ToJWT AuthData
instance FromJWT AuthData
instance FromRow AuthData

-- | Credentials for authentication
data Credentials = Creds
  { login    :: T.Text
  , password :: T.Text
  } deriving (Show, Generic)

$(deriveJSON jsonOptions ''Credentials)

instance ToSchema Credentials

-- | Representation of user in database
data DataBaseUser = DBUser
  { dbuserId        :: Int
  , dbuserUsername  :: T.Text
  , dbuserPassword  :: T.Text
  , dbuserGroup     :: Int
  , dbuserCreatedAt :: UTCTime
  , dbuserUpdatedAt :: UTCTime
  } deriving (Show, Generic)

instance FromRow DataBaseUser

$(deriveJSON jsonOptions ''DataBaseUser)

instance ToSchema DataBaseUser

-- | Response after registration and authentication
newtype TokenResponse =
  TokenR
    { token :: T.Text
    }
  deriving (Show, Generic)

$(deriveJSON jsonOptions ''TokenResponse)

instance ToSchema TokenResponse

-- | Create new user in database
createUser :: Pool Connection
           -> Credentials
           -> IO (Either AppError AuthData)
createUser conns Creds {..}
  | T.length login < 2 = return $ Left IncorrectLoginError
  | T.length password < 6 = return $ Left PasswordTooShortError
  | otherwise =
    handleJust constraintViolation handler $ do
      h <-
        hashPasswordUsingPolicy
          slowerBcryptHashingPolicy {preferredHashAlgorithm = "$2b$"} $
        toBS password
      case h of
        Just hash -> do
          let q =
                "INSERT INTO users (username, password, group_id)\
                  \ VALUES (?, ?, (SELECT id FROM auth_groups WHERE\
                  \ group_name = 'user')) RETURNING id, group_id"
          res <- withResource conns $ \conn -> query conn q (login, hash)
          case res of
            [authData] -> do
              profile' <- createEmptyProfile conns $ authId authData
              case profile' of
                Right _ -> return $ Right authData
                Left e  -> return $ Left e
            _ -> return $ Left DataBaseError
        Nothing -> return $ Left InternalServerError
  where
    handler (UniqueViolation "users_username_key") =
      return $ Left LoginUsedError
    handler (ForeignKeyViolation "users" "users_group_id_fkey") =
      return $ Left DataBaseError
    handler _ = return $ Left DataBaseError


-- | Lookup user in database and check password
authCheck :: Pool Connection
          -> Credentials
          -> IO (Either AppError AuthData)
authCheck conns Creds {..} = do
  let q = "SELECT * FROM users WHERE username = ?"
  res <- withResource conns $ \conn -> query conn q [login]
  case res of
    [userRow] ->
      if validatePassword (toBS $ dbuserPassword userRow) (toBS password)
        then return . Right $ AData (dbuserId userRow) (dbuserGroup userRow)
        else return $ Left WrongPasswordError
    [] -> return $ Left UserNotFountError
    _ -> return $ Left DataBaseError

-- | Convert Text to bytestring
toBS :: T.Text -> C.ByteString
toBS = C.pack . T.unpack

-- | Execute function only for specific auth group or admin user
withGroup :: Int                    -- ^ auth group id
          -> AuthResult AuthData    -- ^ decoded data for current user
          -> IO (Either AppError a) -- ^ required action
          -> IO (Either AppError a) -- ^ result action
withGroup gid (Authenticated AData {..}) action
  | authGroup == 3 = action
  | authGroup == gid = action
  | otherwise = return $ Left AccessDeniedError
withGroup _ _ _ = return $ Left UnauthorizedError
