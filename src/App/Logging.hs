{-# LANGUAGE DeriveGeneric #-}
-- | Logging settings and utilities

module App.Logging
  ( LogMessage(..)
  ) where

import           Data.Aeson
import qualified Data.Text             as T
import           Data.Time             (UTCTime)
import           GHC.Generics
import           System.Log.FastLogger

data LogMessage = LogMessage {
  message        :: !T.Text
  , timestamp    :: !UTCTime
  , level        :: !T.Text
  , lversion     :: !T.Text
  , lenvironment :: !T.Text
} deriving (Eq, Show, Generic)

instance FromJSON LogMessage
instance ToJSON LogMessage where
  toEncoding = genericToEncoding defaultOptions

instance ToLogStr LogMessage where
  toLogStr = toLogStr . encode
