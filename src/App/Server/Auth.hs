{-# LANGUAGE DataKinds #-}
-- | API handlers for authentication and registration

module App.Server.Auth
  (
    -- * auth server
    authServer
  ) where

import           App.Config                 (AppCtx (..), AppM)
import           App.Error
import           App.Model.Auth
import           App.Route.Auth
import           Control.Monad.Reader
import           Control.Monad.Trans        (liftIO)
import qualified Data.ByteString.Lazy.Char8 as C
import qualified Data.Text                  as T
import           Servant
import           Servant.Auth.Server        (CookieSettings, JWTSettings,
                                             SetCookie, acceptLogin, makeJWT)

authServer :: CookieSettings
           -> JWTSettings
           -> ServerT AuthApi AppM
authServer cs jwts
  = authHandler cs jwts :<|>
    regHandler cs jwts

authHandler :: CookieSettings
            -> JWTSettings
            -> Credentials
            -> AppM (Headers '[ Header "Set-Cookie" SetCookie
                              , Header "Set-Cookie" SetCookie
                              ] TokenResponse)
authHandler cs jwts creds = do
  conns <- asks _getConnectionPool

  auth <- liftIO $ authCheck conns creds
  case auth of
    Right userData -> do
      mApplyCookies <- liftIO $ acceptLogin cs jwts userData
      case mApplyCookies of
        Nothing           -> throwErr InternalServerError
        Just applyCookies -> do
          token' <- liftIO $ makeJWT userData jwts Nothing
          case token' of
            Right t -> return . applyCookies $ TokenR { token = T.pack $ C.unpack t }
            Left _ -> throwErr InternalServerError
    Left e -> throwErr e

regHandler :: CookieSettings
           -> JWTSettings
           -> Credentials
           -> AppM (Headers '[ Header "Set-Cookie" SetCookie
                             , Header "Set-Cookie" SetCookie
                             , Header "Location" String
                             ] TokenResponse)
regHandler cs jwts creds = do
  conns <- asks _getConnectionPool
  reg <- liftIO $ createUser conns creds
  case reg of
    Right userData -> do
      mApplyCookies <- liftIO $ acceptLogin cs jwts userData
      case mApplyCookies of
        Nothing           -> throwErr InternalServerError
        Just applyCookies -> do
          token' <- liftIO $ makeJWT userData jwts Nothing
          case token' of
            Right t -> return . applyCookies . addHeader
              ("/profile/" ++ show (authId userData)) $
              TokenR { token = T.pack $ C.unpack t }
            Left _ -> throwErr InternalServerError
    Left e -> throwErr e
