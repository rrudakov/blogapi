{-# LANGUAGE RecordWildCards #-}
-- | API handlers for profile access

module App.Server.Profile
  (
    -- * profile server
    profileServer
  ) where

import           App.Config           (AppCtx (..), AppM)
import           App.Error
import           App.Model.Auth       (AuthData (..), withGroup)
import           App.Model.Profile
import           App.Route.Profile
import           Control.Monad.Reader
import           Control.Monad.Trans  (liftIO)
import           Servant
import           Servant.Auth.Server  (AuthResult (..))

-- | Aggregate profile server
profileServer :: ServerT ProfileApi AppM
profileServer = getProfileByIdHandler :<|> updateProfileHandler

-- | Get profile by id or return error.
getProfileByIdHandler :: Int
                      -> AppM UserProfile
getProfileByIdHandler uid = do
  conns <- asks _getConnectionPool
  liftIO (getProfileById conns uid) >>= handleIOResult


-- | Update user profile. Throw access denied error if user is not
-- admin or try to update foreign profile.
updateProfileHandler :: AuthResult AuthData
                     -> Int
                     -> UserProfile
                     -> AppM NoContent
updateProfileHandler auth@(Authenticated AData {..}) uid uProfile
  | authId == uid || authGroup == 3 = do
    conns <- asks _getConnectionPool
    res <- liftIO . withGroup 1 auth $ updateProfile conns uid uProfile
    handleIOResult res
  | otherwise = throwJSONError err403 AccessDeniedError
updateProfileHandler _ _ _ = throwJSONError err401 UnauthorizedError
