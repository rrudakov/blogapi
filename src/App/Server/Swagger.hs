{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators       #-}
-- | Swagger server

module App.Server.Swagger
  (
    swaggerServer
  ) where

import           App.Config          (AppM)
import           App.Model.Auth      (AuthData)
import           App.Route           (appAPI)
import           App.Route.Swagger   (SwaggerAPI)
import           Data.Swagger
import           Servant
import           Servant.Auth.Server (Auth, JWT)
import           Servant.Swagger

instance (HasSwagger sub) => HasSwagger (Auth '[JWT] AuthData :> sub) where
  toSwagger _ = toSwagger (Proxy :: Proxy sub)


appSwagger :: Swagger
appSwagger = toSwagger appAPI

swaggerServer :: ServerT SwaggerAPI AppM
swaggerServer = return appSwagger
