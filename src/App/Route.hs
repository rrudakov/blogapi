{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
-- | Main routes

module App.Route
  (
    -- * The @API@ type
    API
  , AppAPI
    -- * api for main application
  , api
  , appAPI
  ) where

import           App.Route.Auth
import           App.Route.Profile
import           App.Route.Swagger
import           Servant

type AppAPI = AuthApi :<|> ProfileApi

appAPI :: Proxy AppAPI
appAPI = Proxy

type API = AppAPI :<|> SwaggerAPI

api :: Proxy API
api = Proxy
