-- | Main server
module App.Server where

import           App.Config          (AppM)
import           App.Route           (API, AppAPI)
import           App.Server.Auth
import           App.Server.Profile
import           App.Server.Swagger
import           Servant
import           Servant.Auth.Server (CookieSettings, JWTSettings)


appServer :: CookieSettings -> JWTSettings -> ServerT AppAPI AppM
appServer cs jwts
  = authServer cs jwts :<|>
    profileServer

server :: CookieSettings -> JWTSettings -> ServerT API AppM
server cs jwts = appServer cs jwts :<|> swaggerServer
