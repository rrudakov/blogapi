{-# LANGUAGE DeriveGeneric   #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
-- | Application configuration data types

module App.Config
  ( ApplicationConfig(..)
  , DataBaseConfig(..)
  , AppCtx(..)
  , AppM
  , appConnectionInfo
  ) where

import           Control.Monad.Reader
import           Data.Aeson.TH
import           Data.Pool
import qualified Data.Text                  as T
import           Database.PostgreSQL.Simple
import           GHC.Generics
import           Servant                    (Handler)
import           System.Log.FastLogger
import           Utils.Helpers

data DataBaseConfig =
  DBConfig
    { dbHost     :: T.Text
    , dbUser     :: T.Text
    , dbPassword :: T.Text
    , dbName     :: T.Text
    }
  deriving (Show, Generic)

$(deriveJSON jsonOptions ''DataBaseConfig)

data ApplicationConfig =
  AppConfig
    { databaseConfig :: DataBaseConfig
    , appVersion     :: T.Text
    , appEnvironment :: T.Text
    }
  deriving (Show, Generic)

$(deriveJSON jsonOptions ''ApplicationConfig)

appConnectionInfo :: DataBaseConfig -> ConnectInfo
appConnectionInfo DBConfig {..} =
  ConnectInfo
    { connectHost = T.unpack dbHost
    , connectPort = 5432
    , connectUser = T.unpack dbUser
    , connectDatabase = T.unpack dbName
    , connectPassword = T.unpack dbPassword
    }

data AppCtx =
  AppCtx
    { _getConfig         :: ApplicationConfig
    , _getLogger         :: LoggerSet
    , _getConnectionPool :: Pool Connection
    }

type AppM = ReaderT AppCtx Handler
