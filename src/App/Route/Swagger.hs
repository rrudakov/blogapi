{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
-- | API for serving swagger.json

module App.Route.Swagger
  (
    -- * The @SwaggerAPI@ type
    SwaggerAPI
  ) where

import           Data.Swagger
import           Servant

type SwaggerAPI = "swagger.json" :> Get '[ JSON] Swagger
