{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
-- | All routes related to authentication and registration

module App.Route.Auth
  (
    -- * The @AuthApi@ type
    AuthApi
  ) where

import           App.Model.Auth      (Credentials, TokenResponse)
import           Servant
import           Servant.Auth.Server (SetCookie)

type AuthApi
  = "login"
    :> ReqBody '[JSON] Credentials
    :> PostAccepted '[JSON] (Headers '[ Header "Set-Cookie" SetCookie
                                      , Header "Set-Cookie" SetCookie
                                      ] TokenResponse) :<|>
    "register"
    :> ReqBody '[JSON] Credentials
    :> PostCreated '[JSON] (Headers '[ Header "Set-Cookie" SetCookie
                                     , Header "Set-Cookie" SetCookie
                                     , Header "Location" String
                                     ] TokenResponse)
