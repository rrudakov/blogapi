{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE TypeOperators #-}
-- | Profile API endpoint

module App.Route.Profile
  (
    -- * The @ProfileApi@ type
    ProfileApi
  ) where

import           App.Model.Auth      (AuthData)
import           App.Model.Profile
import           Servant
import           Servant.Auth.Server (Auth, JWT)

-- | Profile API routes
type ProfileApi = "users" :>
  (
    -- /profiles/:id/ GET - get user profile by id
    Capture "id" Int :> Get '[JSON] UserProfile :<|>
    -- /profiles/:id/ PATCH - update user profile (protected)
    Auth '[JWT] AuthData
    :> Capture "id" Int
    :> ReqBody '[JSON] UserProfile
    :> PatchNoContent '[JSON] NoContent
  )
